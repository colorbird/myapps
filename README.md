README.MD



# Paypal payments Donation QuickPay app
![Paypal payments app Preview](vids/quickpay.mp4)




# Augmented Reality Trading Platform Please click the image to see the video

Utilize the power of augmented Reality to visualize trading information from Yahoo Finance Api or your mobile phone/ VR / AR glasses. Have multi monitor display to  enhance trading experience on a single screen.

[![Panotrader](http://img.youtube.com/vi/Fz-0qijMvTY/0.jpg)](https://www.youtube.com/watch?v=Fz-0qijMvTY)

![3D Trading  App preview](https://www.youtube.com/watch?v=Fz-0qijMvTY)

# Interstellar Game


![Interstellar App preview](vids/spacefitlight.mp4)


# Bucket Filling App

![Fitness Bucket App preview](vids/MannfitBucket.mp4)

# Athlete Animation based on Gyroscope

![Athlete Animation preview](vids/KiwiBasicAnimation.mp4)
